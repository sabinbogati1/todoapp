import NavigationService from './navigationService';

export function navigateToProductDetailScreen(params) {
  NavigationService.navigate('ProductDetailScreen', params);
}

export function navigateToDashboardScreen(params) {
  NavigationService.navigate('DashboardScreen', params);
}

export function navigateToFavoriteProductScreen(params) {
  NavigationService.navigate('FavoriteProductScreen', params);
}
