import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import {BottomTabStack} from './BottomTab';
import ProductDetailScreen from 'screens/productDetailScreen/index';
import TodoDashboard from "screens/todoDashboard";

const AppNavigator = createSwitchNavigator({
  todoDashboard: TodoDashboard,
  DashboardScreen: BottomTabStack,
  ProductDetailScreen: ProductDetailScreen,
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
