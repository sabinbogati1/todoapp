import {createStackNavigator} from 'react-navigation-stack';

import DashboardScreen from 'screens/Dashboard';
import FavoriteProductScreen from 'screens/FavoriteProductScreen';
import ProductDetailScreen from 'screens/productDetailScreen';

const DashboardStack = createStackNavigator(
  {
    DashboardScreen: {
      screen: DashboardScreen,
      headerMode: 'none',
      navigationOptions: {
        header: null,
      },
    },
    ProductDetailScreen: {
      screen: ProductDetailScreen,
      navigationOptions: {
        title: `ProductDetailScreen`,
        headerBackTitle: 'A much too long text for back button from B to A',
        headerTruncatedBackTitle: `to A`,
      },
    },
    FavoriteProductScreen: {
      screen: FavoriteProductScreen,
      navigationOptions: {
        title: `FavoriteScreen`,
        headerBackTitle: 'A much too long text for back button from B to A',
        headerTruncatedBackTitle: `to A`,
      },
    },
  },
  {
    cardStyle: {
      backgroundColor: '#F8F9FE',
    },
    transitionConfig,
  },
);

DashboardStack.navigationOptions = ({navigation}) => {
  return {
    tabBarVisible: navigation.state.index !== 1,
  };
};

// const transitionConfig = () => ({
//   transitionSpec: {
//     duration: 300,
//     easing: Easing.out(Easing.poly(4)),
//     timing: Animated.timing,
//   },
//   screenInterpolator: sceneProps => {
//     const {layout, position, scene} = sceneProps;
//     const {index} = scene;

//     const height = layout.initHeight;
//     const translateY = position.interpolate({
//       inputRange: [index - 1, index, index + 1],
//       outputRange: [height, 0, 0],
//     });

//     const opacity = position.interpolate({
//       inputRange: [index - 1, index - 0.99, index],
//       outputRange: [0, 1, 1],
//     });

//     return {opacity, transform: [{translateY}]};
//   },
// });
const transitionConfig = () => ({
  transitionSpec: {
    duration: 400,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing,
  },
  screenInterpolator: sceneProps => {
    const {layout, position, scene} = sceneProps;
    const thisSceneIndex = scene.index;
    const width = layout.initWidth;
    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [width, 0],
    });
    const translateY = 0;

    return {transform: [{translateY}, {translateX}]};
  },
});

export {DashboardStack};
