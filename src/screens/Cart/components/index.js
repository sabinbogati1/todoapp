import React, {Component} from 'react';
import {
  Animated,
  Platform,
  StatusBar,
  Text,
  View,
  TextInput,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import {images} from 'themes';
import {styles} from './styles';
import {SafeAreaHeader, ImageSwiper, ProductCard} from 'components';

const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 100 : 73;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const imageSource =
  'https://5.imimg.com/data5/UK/XB/MY-8612414/axe-motorcycle-bike-racing-riding-gloves-black-color-500x500.jpg';

export default class CartComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        // iOS has negative initial scroll value because content inset...
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      refreshing: false,
    };
  }

  _renderScrollViewContent() {
    const data = Array.from({length: 30});
    return (
      <View style={styles.scrollViewContent}>
        {data.map((_, i) => (
          <View key={i} style={styles.row}>
            <Text>{i}</Text>
          </View>
        ))}
      </View>
    );
  }

  render() {
    // Because of content inset the scroll value will be negative on iOS so bring
    // it back to 0.
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });

    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });

    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, -8],
      extrapolate: 'clamp',
    });

    const opacity = {
      opacity: scrollY.interpolate({
        inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
        outputRange: [0, 0, 1],
      }),
    };

    return (
      <SafeAreaHeader>
        <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
            {useNativeDriver: true},
          )}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.setState({refreshing: true});
                setTimeout(() => this.setState({refreshing: false}), 1000);
              }}
              // Android offset for RefreshControl
              progressViewOffset={HEADER_MAX_HEIGHT}
            />
          }
          // iOS offset for RefreshControl
          contentInset={{
            top: HEADER_MAX_HEIGHT,
          }}
          contentOffset={{
            y: -HEADER_MAX_HEIGHT,
          }}>
          {/* {this._renderScrollViewContent()} */}
          <View
            style={{flex: 1, alignItems: 'center', backgroundColor: '#fff'}}>
            <TouchableOpacity activeOpacity={0.6}>
              <ProductCard
                price={'Rs.2000'}
                image={images.jacket}
                title={'Jacket for winter'}
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.6}>
              <ProductCard
                price={'Rs.800'}
                image={{uri: imageSource}}
                title={'Axe Winter Thermal Waterproof Gloves'}
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.6}>
              <ProductCard
                price={'Rs.2450'}
                image={images.shoeforwoman}
                title={`NIKE Woman's Revolution 3 Running shoe`}
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.6}>
              <ProductCard
                price={'Rs.4500'}
                image={images.jacket3}
                title={'Leather jacket for men (Brown)'}
              />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.6}>
              <ProductCard
                price={'Rs.750'}
                image={{uri: imageSource}}
                title={'Axe Winter Thermal Waterproof Gloves'}
              />
            </TouchableOpacity>
          </View>
        </Animated.ScrollView>
        <Animated.View
          pointerEvents="auto"
          style={[styles.header, {transform: [{translateY: headerTranslate}]}]}>
          <Animated.View
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{translateY: imageTranslate}],
              },
            ]}>
            <ImageSwiper height={300} />
          </Animated.View>
        </Animated.View>
        <Animated.View
          style={[
            styles.bar,
            {
              transform: [{translateY: titleTranslate}],
            },
          ]}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              padding: 15,
              paddingRight: 15,
              marginHorizontal: 15,
              borderWidth: 1,
              borderRadius: 40,
              alignItems: 'center',
              borderColor: '#404040',
              backgroundColor: 'transparent',
            }}>
            <TextInput
              placeholder="Search here..."
              style={{
                paddingHorizontal: 10,
                fontSize: 16,
                width: '90%',
                color: '#404040',
              }}
              placeholderTextColor={'#404040'}
              value={this.props.searchText}
              underlineColorAndroid="transparent"
              onChangeText={this.props.setSearchText}
            />
            <TouchableOpacity
              onPress={this.props.searchFilter}
              hitSlop={{top: 20, left: 20, bottom: 20, right: 20}}>
              <Icon name="ios-search" color={'#404040'} size={20} />
            </TouchableOpacity>
          </View>
          <Animated.View
            style={[
              opacity,
              {
                marginTop: 8,
                borderColor: '#ccc',
                width: '100%',
                borderWidth: 0.5,
              },
            ]}
          />
        </Animated.View>
      </SafeAreaHeader>
    );
  }
}
