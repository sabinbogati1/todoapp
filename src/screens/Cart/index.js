import React, {Component} from 'react';
import {View, SafeAreaView, StatusBar, Text} from 'react-native';

import CartComponent from './components';

class CartContainer extends Component {
  state = {
    searchText: '',
  };

  _setSearchText = searchText => {
    this.setState({
      searchText: searchText,
    });
  };

  _searchFilter = () => {
    console.log('search pressed');
  };
  render() {
    return (
      <CartComponent
        {...this.state}
        {...this.props}
        searchFilter={this._searchFilter}
        setSearchText={this._setSearchText}
      />
    );
  }
}

export default CartContainer;
