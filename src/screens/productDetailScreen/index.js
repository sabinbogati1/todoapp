import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {ImageSwiper} from 'components';
import ShareIcon from 'react-native-vector-icons/Fontisto';
import ArrowIcon from 'react-native-vector-icons/MaterialIcons';
import {Rating, AirbnbRating} from 'react-native-ratings';
import RBSheet from 'react-native-raw-bottom-sheet';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/Ionicons';
import DotIcon from 'react-native-vector-icons/Entypo';
import Specifications from './Specification';
import {navigateToDashboardScreen} from 'navigation/navigationHelpers';

class ProductDetail extends Component {
  static navigationOptions = {header: null};

  constructor(props) {
    super(props);
    this.state = {
      toggleHeartIcon: false,
      toggleAddToCart: false,
    };
  }

  handleOnPress = () => {
    this.RBSheet.open();
  };

  closeRBSheet = () => {
    this.RBSheet.close();
  };

  onPressHeartIcon = () => {
    this.setState(prevState => {
      return {
        toggleHeartIcon: !prevState.toggleHeartIcon,
      };
    });
  };

  handleCartPress = () => {
    this.setState(prevState => {
      return {
        toggleAddToCart: !prevState.toggleAddToCart,
      };
    });
  };

  render() {
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;

    return (
      <SafeAreaView
        style={{flex: 1, backgroundColor: '#FFF'}}
        forceInset={{bottom: 'never', top: 'always'}}>
        <ScrollView style={{flex: 1, backgroundColor: '#efefef'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              paddingVertical: 10,
              paddingHorizontal: 10,
              position: 'absolute',
              zIndex: 1,
            }}>
            <View>
              {/* <Icons name="md-arrow-round-back" size={30} /> */}
              <Icons
                name="ios-arrow-back"
                size={30}
                color="black"
                onPress={() => navigateToDashboardScreen()}
              />
            </View>
            <View style={{flexDirection: 'row'}}>
              <Icon name="shopping-cart" size={25} color="black" />
              <DotIcon name="dots-three-vertical" size={25} color="black" />
            </View>
          </View>

          <ImageSwiper height={height * 0.3} />

          <View
            style={{
              height: 100,
              marginTop: 10,
              paddingHorizontal: 10,
              backgroundColor: '#ffffff',
              borderRadius: 5,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                height: 40,
                marginTop: 5,
              }}>
              <View>
                <Text
                  style={{fontSize: 25, fontWeight: 'bold', color: '#ffa31a'}}>
                  {`  Rs. 500  `}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 20,
                }}>
                <Icon
                  name="heart"
                  color={this.state.toggleHeartIcon ? 'red' : '#D0D0D0'}
                  size={22}
                  style={{marginRight: 5}}
                  onPress={this.onPressHeartIcon}
                />
                <ShareIcon name="share" color={'#D0D0D0'} size={20} />
              </View>
            </View>

            <View>
              <Text style={{fontSize: 18}}>Brown Sofa Jewelry Box</Text>
            </View>

            <View style={{alignItems: 'flex-start'}}>
              <Rating imageSize={12} />
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#ffffff',
              height: 50,
              marginTop: 10,
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 10,
              borderRadius: 5,
            }}>
            <View style={{flex: 0.4}}>
              <Text style={{color: '#D0D0D0', fontSize: 18}}>
                Specifications
              </Text>
            </View>
            <View
              style={{
                flex: 0.6,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 13}}>Brand, Box, Content, SKU</Text>
              </View>
              <View>
                <ArrowIcon
                  name="keyboard-arrow-right"
                  color={'#D0D0D0'}
                  size={30}
                  onPress={this.handleOnPress}
                />
              </View>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#ffffff',
              flexDirection: 'column',
              height: 100,
              marginTop: 10,
              paddingHorizontal: 10,
              borderRadius: 5,
            }}>
            <View style={{height: 30}}>
              <Text style={{color: '#D0D0D0', fontSize: 15}}>
                Ratings & Reviews(7)
              </Text>
            </View>
            <View
              style={{
                height: 70,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: '#D0D0D0', fontSize: 14}}>
                {' '}
                This product has no reviews.
              </Text>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#ffffff',
              flexDirection: 'column',
              height: 150,
              marginTop: 10,
              paddingHorizontal: 10,
              borderRadius: 5,
            }}>
            <View style={{height: 30}}>
              <Text style={{color: '#D0D0D0', fontSize: 15}}>
                Questions about this Product(0)
              </Text>
            </View>
            <View
              style={{
                height: 70,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: '#D0D0D0', fontSize: 14}}>
                {' '}
                This product has no questions yet.
              </Text>
            </View>

            <TouchableOpacity
              style={{
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                borderTopColor: 'lightgrey',
                borderWidth: 1,
                borderEndColor: '#ffffff',
                borderLeftColor: '#ffffff',
                borderBottomColor: '#ffffff',
              }}>
              <Text style={{fontSize: 15, color: '#fc5203'}}>
                Ask Questions
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              paddingHorizontal: 10,
              marginTop: 10,
              backgroundColor: '#ffffff',
              borderRadius: 5,
              marginBottom: 10,
            }}>
            <View style={{height: 20}}>
              <Text style={{color: '#D0D0D0', fontSize: 15}}>
                Product Details
              </Text>
            </View>
            <View style={{paddingLeft: 10, marginTop: 10}}>
              <View style={{paddingLeft: 20, flexDirection: 'row'}}>
                <Text style={{color: 'black', fontSize: 15}}>{'\u2022'}</Text>
                <Text style={{color: 'black', fontSize: 12}}>
                  Hooded Denim Jeans Jacket{' '}
                </Text>
              </View>
              <View style={{paddingLeft: 20, flexDirection: 'row'}}>
                <Text style={{color: 'black', fontSize: 15}}>{'\u2022'}</Text>
                <Text style={{color: 'black', fontSize: 12}}>
                  Sizes: Medium, Large, XL and XXL{' '}
                </Text>
              </View>
              <View style={{paddingLeft: 20, flexDirection: 'row'}}>
                <Text style={{color: 'black', fontSize: 15}}>{'\u2022'}</Text>
                <Text style={{color: 'black', fontSize: 12}}>
                  Rib Sleeves, Hooded{' '}
                </Text>
              </View>
              <View style={{paddingLeft: 20, flexDirection: 'row'}}>
                <Text style={{color: 'black', fontSize: 15}}>{'\u2022'}</Text>
                <Text style={{color: 'black', fontSize: 12}}>
                  {' '}
                  Brand: No Brand{' '}
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={{
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                borderTopColor: 'lightgrey',
                borderWidth: 1,
                borderEndColor: '#ffffff',
                borderLeftColor: '#ffffff',
                borderBottomColor: '#ffffff',
              }}>
              {/* <View style={{ justifyContent:"center", alignItems:"center"}}> */}
              <Text style={{fontSize: 15, color: '#1a9cb7'}}>VIEW ALL</Text>
              {/* </View> */}
            </TouchableOpacity>
          </View>
        </ScrollView>

        <View
          style={{
            flexDirection: 'row',
            height: 50,
            justifyContent: 'flex-end',
            alignItems: 'center',
            paddingRight: 10,
          }}>
          <TouchableOpacity
            style={{
              height: 40,
              width: 120,
              backgroundColor: 'orange',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 10,
            }}>
            <Text style={{color: '#ffffff'}}>Buy Now</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              height: 40,
              width: 120,
              backgroundColor: '#fc5203',
              borderRadius: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={this.handleCartPress}>
            <Text style={{color: '#ffffff'}}>
              {this.state.toggleAddToCart ? 'Added to Cart' : 'Add to Cart'}{' '}
            </Text>
          </TouchableOpacity>
        </View>

        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          duration={250}>
          <Specifications closeRBSheet={this.closeRBSheet} />
        </RBSheet>
      </SafeAreaView>
    );
  }
}

export default ProductDetail;
