import React, {Component} from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import {RNText} from 'components';

class Specification extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{height: 40, marginVertical: 5, flexDirection: 'row'}}>
          <View
            style={{flex: 0.8, justifyContent: 'center', alignItems: 'center'}}>
            <RNText style={{fontSize: 20}}>Specifications</RNText>
          </View>
          <View
            style={{
              flex: 0.2,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            <Icon name="cross" size={30} onPress={this.props.closeRBSheet} />
          </View>
        </View>

        <View
          style={{
            borderColor: 'lightgrey',
            borderWidth: 1,
            marginHorizontal: 10,
          }}
        />

        <View style={{marginTop: 10, paddingHorizontal: 20}}>
          <RNText style={{fontSize: 18}}>Brand</RNText>
          <View style={{paddingHorizontal: 10}}>
            <RNText style={{fontSize: 13}}>{'\u2022'} No Brand</RNText>
          </View>
        </View>
        <View style={{marginTop: 10, paddingHorizontal: 20}}>
          <RNText style={{fontSize: 18}}>SKU</RNText>
          <View style={{paddingHorizontal: 10}}>
            <RNText style={{fontSize: 13}}>{'\u2022'} No Brand</RNText>
          </View>
        </View>
        <View style={{marginTop: 10, paddingHorizontal: 20}}>
          <RNText style={{fontSize: 18}}>Brand</RNText>
          <View style={{paddingHorizontal: 10}}>
            <RNText style={{fontSize: 13}}>{'\u2022'} No Brand</RNText>
          </View>
        </View>
      </View>
    );
  }
}

export default Specification;
