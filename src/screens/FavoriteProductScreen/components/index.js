import React from 'react';
import {StatusBar, View} from 'react-native';
import {SafeAreaHeader, RNText} from 'components';
import {styles} from './styles';

const FavoriteProductComponent = props => {
  return (
    <SafeAreaHeader>
      <StatusBar barStyle="dark-content" backgroundColor="#FFFFFF" />
      <View style={styles.mainContainer}>
        <RNText style={styles.headerText}>This is Favorite Screen</RNText>
      </View>
    </SafeAreaHeader>
  );
};

export default FavoriteProductComponent;
