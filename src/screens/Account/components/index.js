import React from 'react';
import {
  View,
  MaskedViewIOS,
  Text,
  StyleSheet,
  StatusBar,
  Easing,
  Platform,
} from 'react-native';
import Animated from 'react-native-reanimated';

import {images} from 'themes';
import {styles} from './styles';
import {SafeAreaHeader, RNText} from 'components';

class AccountComponent extends React.Component {
  state = {
    loadingProgress: new Animated.Value(0),
    animationDone: false,
  };

  componentDidMount() {
    Animated.timing(this.state.loadingProgress, {
      toValue: 100,
      duration: 1000,
      easing: Easing.linear,
      useNativeDriver: true,
      delay: 400,
    }).start(() => {
      this.setState({animationDone: true});
    });
  }

  render() {
    const colorLayer = this.state.animationDone ? null : (
      <View style={[StyleSheet.absoluteFill, {backgroundColor: '#7F23D9'}]} />
    );
    const whiteLayer = this.state.animationDone ? null : (
      <View style={[StyleSheet.absoluteFill, {backgroundColor: '#FFF'}]} />
    );
    const imageScale = {
      transform: [
        {
          scale: this.state.loadingProgress.interpolate({
            inputRange: [0, 15, 100],
            outputRange: [0.1, 0.06, 16],
          }),
        },
      ],
    };
    const opacity = {
      opacity: this.state.loadingProgress.interpolate({
        inputRange: [0, 25, 50],
        outputRange: [0, 0, 1],
        extrapolate: 'clamp',
      }),
    };
    return Platform.OS === 'ios' ? (
      <View style={{flex: 1}}>
        {colorLayer}
        <MaskedViewIOS
          style={{flex: 1}}
          maskElement={
            <View style={styles.mainContainer}>
              <Animated.Image
                source={images.gloves}
                style={[{width: 100, height: 100}, imageScale]}
                resizeMode="contain"
              />
            </View>
          }>
          {whiteLayer}
          <Animated.View style={[opacity, styles.mainContainer]}>
            <Text> Your app name here</Text>
          </Animated.View>
        </MaskedViewIOS>
      </View>
    ) : (
      <SafeAreaHeader>
        <StatusBar backgroundColor={'#FFFFFF'} barStyle="dark-content" />
        <View style={styles.mainContainer}>
          <Text>This is accounts screen</Text>
        </View>
      </SafeAreaHeader>
    );
  }
}

export default AccountComponent;
