import React, {Component} from 'react';
import {View, SafeAreaView, StatusBar, Text} from 'react-native';

import AccountComponent from './components';

class AccountContainer extends Component {
  render() {
    return <AccountComponent />;
  }
}

export default AccountContainer;
