import React from 'react';
import {View, StatusBar, Image} from 'react-native';

import {images} from 'themes';
import {styles} from './styles';
import {SafeAreaHeader, RNText, FloatingButton} from 'components';

const MessageComponent = props => {
  return (
    <SafeAreaHeader>
      <StatusBar backgroundColor={'#FFFFFF'} barStyle="dark-content" />
      <View style={styles.mainContainer}>
        <Image
          source={images.gloves}
          style={{width: 100, height: 100}}
          resizeMode="contain"
        />
      </View>
      <View
        style={{
          flex: 1,
          marginRight: 60,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <FloatingButton
          style={{bottom: 100}}
          heartButtonCall={props.heartButtonCall}
          thumbButtonCall={props.thumbButtonCall}
          locationPinCall={props.locationPinCall}
        />
      </View>
    </SafeAreaHeader>
  );
};

export default MessageComponent;
