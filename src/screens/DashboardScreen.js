import React from 'react';
import {Image, View} from 'react-native';
import {RNText} from 'components';

const DashboardScreen = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <RNText>Welcome to Dashboard Screen</RNText>
      <Image
        source={{
          uri:
            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/sample_img.png',
        }}
        style={{width: 100, height: 100, margin: 16}}
      />
    </View>
  );
};

export default DashboardScreen;
