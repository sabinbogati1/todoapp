import React from 'react';
import {Platform, Animated} from 'react-native';
import DashboardComponent from './components';
import {
  navigateToProductDetailScreen,
  navigateToFavoriteProductScreen,
} from 'navigation/navigationHelpers';

const HEADER_MAX_HEIGHT = 300;

class DashboardContainer extends React.Component {
  state = {
    searchText: '',
    scrollY: new Animated.Value(
      // iOS has negative initial scroll value because content inset...
      Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
    ),
    refreshing: false,
  };

  render() {
    return (
      <DashboardComponent
        {...this.state}
        {...this.props}
        _searchFilter={this._searchFilter}
        _setSearchText={this._setSearchText}
        heartButtonCall={this._heartButtonCall}
        gotoProductDetailNews={this.gotoProductDetailNews}
      />
    );
  }

  _heartButtonCall = () => {
    navigateToFavoriteProductScreen();
  };

  gotoProductDetailNews = id => {
    console.log('onEachProductPress id :', id);
    navigateToProductDetailScreen();
  };

  _setSearchText = searchText => {
    this.setState({
      searchText: searchText,
    });
  };

  _searchFilter = () => {
    console.log('search pressed');
  };
}

export default DashboardContainer;
