import React from 'react';
import {
  View,
  ScrollView,
  StatusBar,
  Image,
  TouchableOpacity,
  Platform,
  Animated,
  RefreshControl,
  TextInput,
} from 'react-native';
// import Animated from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Ionicons';

import {styles} from './styles';
import {
  RNText,
  ProductCard,
  SearchButton,
  FloatingButton,
  SafeAreaHeader,
  ImageSwiper,
} from 'components';
import {images, layout} from 'themes';

const DashboardComponent = props => {
  const {_setSearchText, _searchFilter, searchText} = props;
  const dummyImageURL =
    'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png';
  const imageSource =
    'https://5.imimg.com/data5/UK/XB/MY-8612414/axe-motorcycle-bike-racing-riding-gloves-black-color-500x500.jpg';

  const HEADER_MAX_HEIGHT = layout.window.height <= 600 ? 200 : 250;
  const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 100 : 66;
  const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
  // Because of content inset the scroll value will be negative on iOS so bring
  // it back to 0.
  const scrollY = Animated.add(
    props.scrollY,
    Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
  );
  const headerTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',
  });
  const imageTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 100],
    extrapolate: 'clamp',
  });

  // const titleScale = scrollY.interpolate({
  //   inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
  //   outputRange: [1, 1, 0.8],
  //   extrapolate: 'clamp',
  // });
  const titleTranslate = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [0, 0, -8],
    extrapolate: 'clamp',
  });

  const opacity = {
    opacity: scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, 1],
    }),
  };

  return (
    <SafeAreaHeader>
      <StatusBar backgroundColor={'#FFFFFF'} barStyle="dark-content" />
      <Animated.ScrollView
        style={styles.fill}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {y: props.scrollY}}}],
          {useNativeDriver: true},
        )}
        // refreshControl={
        //   <RefreshControl
        // refreshing={props.refreshing}
        // onRefresh={() => {
        //   this.setState({refreshing: true});
        //   setTimeout(() => this.setState({refreshing: false}), 1000);
        // }}
        // Android offset for RefreshControl
        //   progressViewOffset={HEADER_MAX_HEIGHT}
        // />
        // }
        // iOS offset for RefreshControl
        contentInset={{
          top: HEADER_MAX_HEIGHT,
        }}
        contentOffset={{
          y: -HEADER_MAX_HEIGHT,
        }}>
        <ScrollView
          horizontal
          style={{
            marginTop:
              Platform.OS === 'ios'
                ? -40
                : layout.window.height <= 600
                ? 200
                : 250,
          }}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingHorizontal: 15,
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              backgroundColor: '#fff',
            }}>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews(1)}>
              <ProductCard
                price={'Rs.2000'}
                image={images.jacket}
                defaultSource={{uri: dummyImageURL}}
                title={'Jacket for winter'}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews(2)}>
              <ProductCard
                price={'Rs.800'}
                image={{uri: imageSource}}
                defaultSource={{uri: dummyImageURL}}
                title={'Axe Winter Thermal Waterproof Gloves'}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews(3)}>
              <ProductCard
                price={'Rs.2450'}
                image={images.shoeforwoman}
                defaultSource={{uri: dummyImageURL}}
                title={`NIKE Woman's Revolution 3 Running shoe`}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews(4)}>
              <ProductCard
                price={'Rs.4500'}
                image={images.jacket3}
                defaultSource={{uri: dummyImageURL}}
                title={'Leather jacket for men (Brown)'}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews(5)}>
              <ProductCard
                price={'Rs.750'}
                image={{uri: imageSource}}
                defaultSource={{uri: dummyImageURL}}
                title={'Axe Winter Thermal Waterproof Gloves'}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={{flex: 1, paddingHorizontal: 15, paddingVertical: 15}}>
          <RNText style={{fontSize: 20, color: '#1a75ff'}}>
            {' '}
            Most Popular{' '}
          </RNText>
          <View
            style={{
              flex: 1,
              paddingTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Biker Gloves`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  111{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.gloves} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flex: 1, paddingHorizontal: 15, paddingVertical: 15}}>
          <RNText style={{fontSize: 20, color: '#1a75ff'}}>
            {' '}
            Most Popular{' '}
          </RNText>
          <View
            style={{
              flex: 1,
              paddingTop: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Biker Gloves`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  111{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.gloves} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('Nike Shoe')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Woman's NIKE shoe`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  121{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image
                  source={images.shoeforwoman}
                  style={styles.imageViewStyle}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cardStyles}
              activeOpacity={0.6}
              onPress={() => props.gotoProductDetailNews('gloves')}>
              <View style={styles.flex6}>
                <RNText style={styles.titleStyles}>
                  {' '}
                  {`Men's Leather jacket`}
                </RNText>
                <RNText style={styles.descStyles}>
                  {' '}
                  11{' '}
                  <RNText style={styles.innerDescStyles}>
                    people want this{' '}
                  </RNText>
                </RNText>
              </View>
              <View style={styles.viewStyle}>
                <Image source={images.jacket3} style={styles.imageStyle} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Animated.ScrollView>

      <Animated.View
        pointerEvents="auto"
        style={[styles.header, {transform: [{translateY: headerTranslate}]}]}>
        <Animated.View
          style={[
            styles.backgroundImage,
            {
              opacity: imageOpacity,
              transform: [{translateY: imageTranslate}],
            },
          ]}>
          <ImageSwiper height={300} />
        </Animated.View>
      </Animated.View>
      <Animated.View
        style={[
          styles.bar,
          {
            transform: [{translateY: titleTranslate}],
          },
        ]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            padding: Platform.OS === 'ios' ? 15 : 0,
            paddingRight: 15,
            marginHorizontal: 15,
            borderWidth: 1,
            borderRadius: 40,
            alignItems: 'center',
            borderColor: '#404040',
            backgroundColor: 'transparent',
          }}>
          <TextInput
            placeholder="Search here..."
            style={{
              paddingHorizontal: 10,
              fontSize: 16,
              width: '90%',
              color: '#404040',
            }}
            placeholderTextColor={'#404040'}
            value={props.searchText}
            underlineColorAndroid="transparent"
            onChangeText={props.setSearchText}
          />
          <TouchableOpacity
            onPress={props.searchFilter}
            hitSlop={{top: 20, left: 20, bottom: 20, right: 20}}>
            <Icon name="ios-search" color={'#404040'} size={20} />
          </TouchableOpacity>
        </View>
        <Animated.View
          style={[
            opacity,
            {
              marginTop: 8,
              borderColor: '#ccc',
              width: '100%',
              borderWidth: 0.5,
            },
          ]}
        />
      </Animated.View>
      <View
        style={{
          // flex: 1,
          marginRight: 60,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}>
        <FloatingButton
          style={{bottom: 100}}
          heartButtonCall={'FavoriteProductScreen'}
          thumbButtonCall={'ProductDetailScreen'}
          locationPinCall={props.locationPinCall}
        />
      </View>
    </SafeAreaHeader>
  );
};

export default DashboardComponent;
