import {Platform} from 'react-native';
import {layout} from 'themes';

const HEADER_MAX_HEIGHT = layout.window.height <= 600 ? 200 : 250;
console.log(layout.window.height, ' height');

export const styles = {
  fill: {
    flex: 1,
  },
  safeAreaView: {
    flex: 1,
    // backgroundColor: '#efefef',
    backgroundColor: '#FFFFFF',
  },
  mainContainer: {
    marginHorizontal: 15,
  },
  flex1: {
    flex: 1,
  },
  container: {
    height: 220,
    width: 200,
    paddingLeft: 5,
    paddingRight: 10,
    paddingBottom: 15,
  },
  imageBackground: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 20,
    height: null,
    width: null,
  },
  backgroundImageStyles: {
    borderRadius: 5,
    backgroundColor: '#ccc',
  },
  titleView: {width: 160, paddingHorizontal: 10},
  title: {color: 'black'},
  headerText: {
    lineHeight: 24,
    color: '#FFFFFF',
  },
  horizontalLine: {
    width: 80,
    marginVertical: 2,
    borderBottomColor: '#ddd',
    borderBottomWidth: 0.5,
  },
  imageBackgroundWrapper: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderRadius: 20,
    backgroundColor: 'rgba(0, 0, 0, .4)',
  },
  cardStyles: {
    flex: 1,
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    marginHorizontal: 5,
    marginVertical: 5,
  },
  titleStyles: {textAlign: 'center', fontSize: 14, color: '#000'},
  descStyles: {textAlign: 'center', fontSize: 12, color: '#000'},
  innerDescStyles: {
    textAlign: 'center',
    fontSize: 12,
    color: '#808080',
  },
  imageStyle: {
    width: 60,
    flex: 1,
    height: 50,
    resizeMode: 'cover',
  },
  imageViewStyle: {
    width: 60,
    flex: 1,
    height: 50,
    resizeMode: 'contain',
  },
  viewStyle: {
    flex: 0.4,
    alignItems: 'center',
  },
  flex6: {flex: 0.6},
  content: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  bar: {
    backgroundColor: 'transparent',
    marginTop: Platform.OS === 'ios' ? 28 : 15,
    // height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: Platform.OS === 'ios' ? 20 : 0,
    left: 0,
    right: 0,
  },
  title: {
    color: 'white',
    fontSize: 20,
  },
  scrollViewContent: {
    // iOS uses content inset, which acts like padding.
    paddingTop: Platform.OS !== 'ios' ? HEADER_MAX_HEIGHT : 0,
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
};
