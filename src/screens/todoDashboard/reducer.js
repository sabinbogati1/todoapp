import { ADD_TASK, DELETE_TASK, COMPLETED_TASK, CLEAR_COMPLETED_TASK } from "./actionType";

const initialState = {
    taskList: [
        { id: 2, taskName: "Do some home work", "date": "2019-12-15T13:17:22.247Z" },
        { id: 3, taskName: "Push to git" , "date": "2019-12-15T13:17:22.247Z"},
        { id: 1, taskName: "Travel West", "date": "2019-12-15T13:17:22.247Z" },
    ],
    pendingTaskList: [],
    completedTaskList: []
}


export default (state = initialState, action) => {

    switch (action.type) {
        case ADD_TASK:
            return {
                ...state,
                taskList: [action.payload, ...state.taskList]
            }

        case DELETE_TASK:
            let taskList = [...state.taskList];
            let filterData = taskList.filter((task) => {
                  return task.id != action.payload
                });

                return {
                    ...state,
                    taskList: [...filterData]
                }

        case COMPLETED_TASK:
            taskList = [...state.taskList];
            let pendingTask = taskList.filter((task) => {
                return task.id != action.payload.taskId
              });

            let completedTask = taskList.filter((task) => {
                return task.id == action.payload.taskId
              });  

              completedTask[0].date = action.payload.completedDate

              return {
                ...state,
                taskList: [...pendingTask],
                completedTaskList: [...completedTask, ...state.completedTaskList ]
            }

        case CLEAR_COMPLETED_TASK: 
            let completedTasks = [...state.completedTaskList];
            let filterCompletedTasks = completedTasks.filter(task=>{
                return task.id != action.payload
            })
            return {
                ...state,
                completedTaskList:[...filterCompletedTasks]
            }

        default:
            return state;
    }

}