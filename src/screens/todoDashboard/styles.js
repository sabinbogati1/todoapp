import {StyleSheet, Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export const styles = {
  container: {
    flex: 1,
  },
  header: {
    padding: 10,
    alignItems: 'flex-end',
    backgroundColor: '#74B9FF',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  headerText: {
    fontWeight: 'bold',
    color: '#EAF0F1',
    fontSize: 25,
    marginTop: 5,
  },
  textInput: {
    height: 40,
    width: 300,
    borderRadius: 5,
    backgroundColor: '#fff',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 10,
    padding: 10,
  },
  addButton: {
    height: 30,
    width: 50,
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  msgTextWrapper: {
    alignItems: 'center',
    marginVertical: 30,
  },
  msgText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  taskTypeText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 15,
  },
  hScrollView: {
    flex: 1,
    backgroundColor: '#74B9FF',
    borderRadius: 5,
  },
  showTaskWrapper:{
    flex: 1,
    borderWidth: 2,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
    paddingVertical: 10,
    backgroundColor: '#EAF0F1'
  },
  taskInfoWrapper:{
    flex: 0.7, 
    padding: 10
  },
  timeInfoText:{
    fontSize: 10, fontStyle: 'italic'
  },
  showTaskBtnWrapper:{
    flex: 0.3, flexDirection: 'row'
  },
  showTaskDelBtn:{
    borderRadius: 5,
    borderColor: 'red',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  showTaskDoneBtn:{
    borderRadius: 5,
    borderColor: 'red',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  comTskWrapper:{
    flex: 1,
    borderWidth: 2,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    marginVertical: 5,
    paddingVertical: 10,
    backgroundColor: '#EAF0F1',
  },
  comBtnWrapper:{
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  comTskDelBtn:{
    borderRadius: 5,
    borderColor: 'red',
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginRight: 10,
  },
  showTskScrollView:{
    width, padding: 5
  },
  comTskScrollView:{
    width,
    backgroundColor: '#74B9FF',
    borderRadius: 5,
    padding: 5,
  }
};
