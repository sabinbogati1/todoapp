import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Animated,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions,
  UIManager,
  StatusBar,
  Platform,
  LayoutAnimation,
} from 'react-native';
import moment from 'moment';
import uuid4 from 'uuid/v4';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  addTask,
  deleteTask,
  completedTask,
  clearCompletedTask,
} from './actions';
import DeleteIcon from 'react-native-vector-icons/AntDesign';
import DoneIcon from 'react-native-vector-icons/MaterialIcons';
import {styles} from './styles';

class ToDoDashboard extends Component {
  constructor(props) {
    super(props);

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    this.state = {
      headerAnimation: new Animated.Value(0),
      textAnimation: new Animated.Value(0),
      sizeAnimation: new Animated.Value(0),
      taskName: '',
      isScrollAtEnd: false,
      xOffset: new Animated.Value(0),
    };
  }

  onChangeText = text => {
    this.setState({
      taskName: text,
    });
  };

  handlePress = () => {
    const {taskName, headerAnimation } = this.state
    if (taskName) {
      const id = uuid4();
      var date = new Date();
      const newTask = {id: id, taskName: taskName, date: date};
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
      this.props.addTask(newTask);
      this.setState({
        taskName: '',
      });
    } else {
      Animated.timing(headerAnimation, {
        toValue: 1,
        duration: 30,
      }).start(() => {
        Animated.spring(headerAnimation, {
          toValue: 0,
          friction: 1,
          tension: 300,
        }).start();
      });
    }
  };

  handleTaskDelete = taskId => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.deleteTask(taskId);
  };

  handleTaskComplete = taskId => {
    var completedDate = new Date();
    const data = {taskId, completedDate};
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.completedTask(data);
  };

  showtask = () => {
    const {todo} = this.props;
    return todo.taskList.map(task => {
      var diffDate = moment(task.date).fromNow();

      return (
        <View key={task.id} style={styles.showTaskWrapper}>
          <View style={styles.taskInfoWrapper}>
            <Text>{task.taskName}</Text>
            <Text style={styles.timeInfoText}>Added {diffDate}</Text>
          </View>

          <View style={styles.showTaskBtnWrapper}>
            <TouchableOpacity
              style={styles.showTaskDelBtn}
              onPress={() => this.handleTaskDelete(task.id)}>
              <DeleteIcon name="delete" size={20} color="red" />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.showTaskDoneBtn}
              onPress={() => this.handleTaskComplete(task.id)}>
              <DoneIcon name="done" size={24} color="#2ecc72" />
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  removeCompletedTask = taskId => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.clearCompletedTask(taskId);
  };

  showCompletedTask = () => {
    const {todo} = this.props;
    return todo.completedTaskList.map(task => {
      var diffDate = moment(task.date).fromNow();

      return (
        <View key={task.id} style={styles.comTskWrapper}>
          <View style={{flex: 0.7, padding: 10}}>
            <Text>{task.taskName}</Text>
            <Text style={styles.timeInfoText}>Completed {diffDate}</Text>
          </View>

          <View style={styles.comBtnWrapper}>
            <TouchableOpacity
              onPress={() => this.removeCompletedTask(task.id)}
              style={styles.comTskDelBtn}>
              <DeleteIcon name="delete" size={20} color="red" />
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  handleScroll = event => {
    if (event.nativeEvent.contentOffset.x == 0) {
      this.setState({
        isScrollAtEnd: false,
      });
    }

    if (event.nativeEvent.contentOffset.x == 360) {
      this.setState({
        isScrollAtEnd: true,
      });
    }
  };

  render() {
    const {taskName, isScrollAtEnd, headerAnimation, sizeAnimation} = this.state;
    const {taskList, completedTaskList} = this.props.todo;
    const title = isScrollAtEnd ? 'Completed Task' : 'Pending Task';
    const taskCount = isScrollAtEnd
      ? completedTaskList.length
      : taskList.length;

    const headerInterpolation = headerAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 10],
    });

    const sizeInterpolation = sizeAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [100, 200],
    });

    const sizeStyles = {
      transform: [
        {
          scaleY: sizeAnimation,
        },
      ],
    };

    const animatedHeader = {
      transform: [
        {
          translateX: headerInterpolation,
        },
      ],
    };

    return (
      <KeyboardAvoidingView style={styles.container}>
        <StatusBar backgroundColor="#74B9FF" barStyle="light-content" />
        <Animated.View style={[styles.header]}>
          <Animated.Text style={[styles.headerText]}> TO DO APP</Animated.Text>

          <Animated.View style={[animatedHeader]}>
            <TextInput
              style={styles.textInput}
              placeholder="Enter New task"
              value={taskName}
              onChangeText={text => this.onChangeText(text)}
            />
          </Animated.View>
          <TouchableOpacity onPress={this.handlePress} style={styles.addButton}>
            <Text style={{color: '#fff'}}> Add </Text>
          </TouchableOpacity>
        </Animated.View>

        <View style={{flex: 3}}>
          <View style={styles.msgTextWrapper}>
            <Animatable.Text
              animation="pulse"
              easing="ease-out"
              iterationCount="infinite"
              style={styles.msgText}>
              {' '}
              Let's get some work done!
            </Animatable.Text>
          </View>

          <Text style={styles.taskTypeText}>
            {title}({taskCount}){' '}
          </Text>
          <ScrollView
            horizontal
            bounces
            pagingEnabled
            style={styles.hScrollView}
            onScroll={this.handleScroll}>
            <ScrollView
              style={{flex: 1}}
              contentContainerStyle={styles.showTskScrollView}>
              {this.showtask()}
            </ScrollView>

            <ScrollView
              style={{flex: 1}}
              contentContainerStyle={styles.comTskScrollView}>
              {this.showCompletedTask()}
            </ScrollView>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

function mapStateToProps(state) {
  return {
    todo: state.todoReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {addTask, deleteTask, completedTask, clearCompletedTask},
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoDashboard);
