import { ADD_TASK, DELETE_TASK, COMPLETED_TASK, CLEAR_COMPLETED_TASK } from "./actionType";


export const addTask = task => {
    return {
        type:ADD_TASK,
        payload:task
    }
}

export const deleteTask = taskId => {
    return {
        type:DELETE_TASK,
        payload: taskId
    }
}


export const completedTask = (data) => {
    return {
        type: COMPLETED_TASK,
        payload: data
    }
}

export const clearCompletedTask = taskId =>{
    return {
        type:CLEAR_COMPLETED_TASK,
        payload:taskId
    }
}