import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen'
import { PersistGate } from 'redux-persist/es/integration/react';
import AppNavigator from './navigation';
import createStore from './redux/store';

const { store, persistor } = createStore();

// const App = () => {
//   return (
//     <Provider store={store}>
//       <AppNavigator />
//     </Provider>
//   );
// };

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    SplashScreen.hide();
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;

