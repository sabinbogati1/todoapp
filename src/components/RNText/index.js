import React, {Component} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';
// import { nunitoItalic, nunitoRegular } from '../../themes/styles';

function applyLetterSpacing(string, letterSpacing) {
  if (
    Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version <= 16) ||
    letterSpacing === 0
  ) {
    return string;
  }

  return string.split('').join('\u200A'.repeat(letterSpacing));
}

const styles = StyleSheet.create({
  defaultStyle: {
    // fontFamily: 'Varela Round',
  },
});

export class RNText extends Component {
  render() {
    return (
      <Text {...this.props} style={[this.props.style]}>
        {this.props.children}
      </Text>
    );
  }
}
