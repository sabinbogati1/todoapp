import React from 'react';
import {View, Text} from 'react-native';

import {styles} from './styles';

const EmptyMessageList = props => {
  return (
    <View style={styles.emptyListView}>
      <Text style={[styles.emptyListText, {color: 'black'}]}>
        {' '}
        {props.title}{' '}
      </Text>
    </View>
  );
};

export {EmptyMessageList};
