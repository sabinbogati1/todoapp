import {StyleSheet} from 'react-native';

// import { colors } from 'themes';
// import { productSansRegular } from 'themes/styles';

export const styles = StyleSheet.create({
  emptyListText: {
    fontSize: 18,
    marginTop: 20,
    textAlign: 'center',
  },
  emptyListView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
