import React from 'react';
import Animated from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Ionicons';
import {View, TextInput, TouchableOpacity} from 'react-native';

import {styles} from './styles';
const HEADER_HEIGHT = 50;

const SearchButton = props => {
  return (
    <Animated.View
      style={[
        styles.searchMainView,
        {
          height: HEADER_HEIGHT,
          overflow: 'hidden',
          transform: [{translateY: props.headerY}],
        },
      ]}>
      <View style={styles.searchInnerView}>
        <TextInput
          placeholder="Search here..."
          style={{
            paddingHorizontal: 10,
            fontSize: 16,
            width: '90%',
            color: 'black',
          }}
          placeholderTextColor={'#808080'}
          value={props.searchText}
          underlineColorAndroid="transparent"
          onChangeText={props.setSearchText}
        />
        <TouchableOpacity
          onPress={props.searchFilter}
          hitSlop={{top: 20, left: 20, bottom: 20, right: 20}}>
          <Icon name="ios-search" color={'#1a75ff'} size={20} />
        </TouchableOpacity>
      </View>
    </Animated.View>
  );
};
export {SearchButton};
