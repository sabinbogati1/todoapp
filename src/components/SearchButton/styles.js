import {StyleSheet, Platform} from 'react-native';

export const styles = StyleSheet.create({
  searchMainView: {
    marginTop: Platform.OS === 'ios' ? 45 : 0,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 1000,
    elevation: 1000,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInnerView: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 40,
    alignItems: 'center',
    borderColor: '#1a75ff',
    backgroundColor: '#efefef',
  },
});
