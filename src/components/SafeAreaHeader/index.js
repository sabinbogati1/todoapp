import React from 'react';
import {SafeAreaView, StatusBar} from 'react-navigation';
import {styles} from './styles';

const SafeAreaHeader = props => (
  <SafeAreaView
    forceInset={{bottom: 'never', top: 'always'}}
    style={styles.safeAreaView}>
    {props.children}
  </SafeAreaView>
);

export {SafeAreaHeader};
