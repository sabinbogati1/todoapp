import {colors} from 'themes/colors';

export const styles = {
  safeAreaView: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
};
