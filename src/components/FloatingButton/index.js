import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import {Animated, View, TouchableWithoutFeedback} from 'react-native';

import {styles} from './styles';
import NavigationService from 'navigation/navigationService';

class FloatingButton extends React.Component {
  animatedValue = new Animated.Value(0);

  toggleMenu = () => {
    const toValue = this.open ? 0 : 1;
    Animated.spring(this.animatedValue, {
      toValue,
      friction: 5,
    }).start();

    this.open = !this.open;
  };

  navigateTo = route => {
    this.toggleMenu();
    console.log({route});
    if (route) {
      setTimeout(() => NavigationService.navigate(route), 500);
    }
  };

  render() {
    const heartStyle = {
      transform: [
        {scale: this.animatedValue},
        {
          translateY: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -200],
          }),
        },
      ],
    };
    const thumbStyle = {
      transform: [
        {scale: this.animatedValue},
        {
          translateY: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -140],
          }),
        },
      ],
    };
    const pinStyle = {
      transform: [
        {scale: this.animatedValue},
        {
          translateY: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -80],
          }),
        },
      ],
    };

    const rotation = {
      transform: [
        {
          rotate: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '45deg'],
          }),
        },
      ],
    };

    const opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 0, 1],
    });

    return (
      <View style={[styles.mainContainer, this.props.style]}>
        <TouchableWithoutFeedback
          onPress={() => this.navigateTo(this.props.heartButtonCall)}>
          <Animated.View
            style={[styles.smallButton, styles.secondary, heartStyle, opacity]}>
            <Icon name="hearto" size={25} color="#F02A4B" />
          </Animated.View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => this.navigateTo(this.props.thumbButtonCall)}>
          <Animated.View
            style={[styles.smallButton, styles.secondary, thumbStyle, opacity]}>
            <Entypo name="thumbs-up" size={25} color="#F02A4B" />
          </Animated.View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={this.navigateTo(this.props.pinButtonCall)}>
          <Animated.View
            style={[styles.smallButton, styles.secondary, pinStyle, opacity]}>
            <Entypo name="location-pin" size={25} color="#F02A4B" />
          </Animated.View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={this.toggleMenu}>
          <Animated.View style={[styles.button, styles.menu, rotation]}>
            <Icon name="plus" size={25} color="#FFF" />
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

export default FloatingButton;
