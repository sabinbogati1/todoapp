import React from 'react';
import {View, Text, ImageBackground} from 'react-native';

import {colors} from 'themes';
import {styles} from './styles';

const ProductCard = props => {
  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMode="cover"
        source={props.image}
        style={styles.imageBackground}>
        <View style={styles.imageBackgroundWrapper}>
          <Text numberOfLines={2} style={[styles.headerText, {fontSize: 18}]}>
            {props.title}
          </Text>
          <View style={styles.horizontalLine} />
          <Text style={{fontSize: 12, color: colors.WHITE}}>{props.price}</Text>
        </View>
      </ImageBackground>
    </View>
  );
};

export {ProductCard};
