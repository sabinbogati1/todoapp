import {StyleSheet, Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  container: {
    height: 220,
    width: width / 2,
    marginVertical: 10,
    backgroundColor: '#fff',
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  imageBackground: {
    flex: 1,
    justifyContent: 'flex-end',
    borderRadius: 20,
    height: null,
    width: null,
  },
  backgroundImageStyles: {
    borderRadius: 5,
    backgroundColor: '#ccc',
  },
  titleView: {width: 160, paddingHorizontal: 10},
  title: {color: 'black'},
  headerText: {
    lineHeight: 24,
    color: '#FFFFFF',
  },
  horizontalLine: {
    width: 80,
    marginVertical: 2,
    borderBottomColor: '#ddd',
    borderBottomWidth: 0.5,
  },
  imageBackgroundWrapper: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 75,
    backgroundColor: 'rgba(0, 0, 0, .4)',
  },
});
